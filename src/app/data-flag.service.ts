import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Subject } from 'rxjs';
import {LocalStorageService} from '@app/-common/localStorage.service';


@Injectable({
  providedIn: 'root'
})
export class DataFlagService {

  constructor(private localStorageService: LocalStorageService) { }

  private responder = new BehaviorSubject<any>(this.localStorageService.getItem('responder_flag'));
  responderFlag = this.responder.asObservable();


  get getResponderFlag(): any {
    return this.responder.value;
  }

  changeResponderFlag(flag) {
    this.localStorageService.setItem('responder_flag', flag);
    const responderRoute = flag === 1 ? '/responder-listing' : '/hr-listing';
    this.localStorageService.setItem('responder_route', responderRoute);
    this.responder.next(flag);
  }
}
