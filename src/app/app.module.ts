import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {HttpClientModule} from '@angular/common/http';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import { Network } from '@ionic-native/network/ngx';

import { IonicStorageModule } from '@ionic/storage';


import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';

import {CoreModule} from './-common/core.module';
import {InitModule} from './init/init.module';
import {NetworkService} from '@app/-common/network.service';
import {ApiService} from '@app/api.service';

import {IonicSignaturePadModule, IonicsignaturepadProvider} from 'ionicsignaturepad';


@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        IonicStorageModule.forRoot({
            name: '__rmspwa',
            driverOrder: ['websql', 'indexeddb', 'sqlite', 'localstorage']
          }),
        HttpClientModule,
        AppRoutingModule,
        CoreModule.forRoot(),
        InitModule,
        ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
        IonicSignaturePadModule
    ],
    providers: [
        StatusBar,
        SplashScreen,
        ApiService,
        Network,
        NetworkService,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
