import {NgModule} from '@angular/core';

import {IonicModule} from '@ionic/angular';

import {LoginPageRoutingModule} from './login-routing.module';

import {LoginPage} from './login.page';

import {SharedModule} from '@app/-common/shared.module';


@NgModule({
    imports: [
        SharedModule,
        IonicModule,
        LoginPageRoutingModule
    ],
    declarations: [LoginPage],
    // providers: [AuthenticationService]
})
export class LoginPageModule {
}
