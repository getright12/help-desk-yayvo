import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {ToastController} from '@ionic/angular';

import {UserService} from '@app/-common/repositories/user/user.service';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';

import { NetworkService } from '@app/-common/network.service';
import { OfflineStorageService } from '@app/-common/offlineStorage.service';
import { LocalStorageService } from '@app/-common/localStorage.service';
import {ApiService} from '@app/api.service';
import {NotificationsService} from '@app/notifications.service';
import {AuthenticationService} from '@app/authentication.service';
// import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    employeeid: number = null;
    verfication = false;
    loggedInUser = null;
    verifyCode: number =  null;
    disabled = false;
    vdisabled = false;
    processing = false;
    vProcessing = false;
    returnUrl: string;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private apiService: ApiService,
        private router: Router,
        private toastController: ToastController,
        private userService: UserService,
        private networkService: NetworkService,
        private offlineStorageService: OfflineStorageService,
        private localStorageService: LocalStorageService,
        private notificationsService: NotificationsService,
        private authenticationService: AuthenticationService
    ) {
        // redirect to home if already logged in
        // if (this.authenticationService.currentUserValue) {
        //     this.router.navigate(['/']);
        // }
    }

    ionViewWillEnter() {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/dashboard']);
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/dashboard';
    }

    async loginHandler() {
        this.disabled = true;
        this.processing = true;
        
        if (!this.employeeid) {
            this.notificationsService.alert('Error', 'Please enter employee ID');
            this.disabled = false;
            this.processing = false;
            return;
        }

        const loginUser = await this.apiService.getLoginInfo(this.loginParams());
        
        if (loginUser.returnStatus.status !== 'SUCCESS') {
            console.log('error condition', loginUser);
            this.notificationsService.alert('Error', loginUser.returnStatus.message);
            this.disabled = false;
            this.processing = false;
            return;
        }

        this.verfication = true;
        this.loggedInUser = loginUser;
        this.disabled = false;
        this.processing = false;
    }

    async onVerifyToken() {
        this.vdisabled = true;
        this.vProcessing = true;
        // FOR DEVELOPMENT
        if (this.loggedInUser.tcsOneOTP === this.verifyCode || this.verifyCode == 12345) {
            this.loggedInUser.isLogin = 1;
            this.loggedInUser.employeeId = this.employeeid;
            this.authenticationService.setlLoginUser(this.loggedInUser).then((res) => {
                this.verfication = false;
                this.notificationsService.toast(this.loggedInUser.returnStatus.message);
                this.router.navigateByUrl(this.returnUrl);
                this.vProcessing = false;
                this.vdisabled = false;
            });
        } else {
            this.notificationsService.toast('Invalid OTP Code', 'Error');
            this.vdisabled = false;
            this.vProcessing = false;
            return false;
        }

        // FOR PRODUCTION
        // if (this.loggedInUser.tcsOneOTP !== this.verifyCode) {
        //     this.notificationsService.toast('Invalid OTP Code', 'Error');
        //     this.vdisabled = false;
        //     this.vProcessing = false;
        //     return false;
        // }

        // this.loggedInUser.isLogin = 1;
        // this.loggedInUser.employeeId = this.employeeid;
        // this.authenticationService.setlLoginUser(this.loggedInUser).then((res) => {
        //     this.verfication = false;
        //     this.notificationsService.toast(this.loggedInUser.returnStatus.message);
        //     this.router.navigateByUrl(this.returnUrl);
        //     this.vProcessing = false;
        //     this.vdisabled = false;
        // });
    }

    loginParams() {
       return {
            tcsLoginRequest:
            {
              employeeNumber: this.employeeid,
              applicationId: '1'
            }
        };
    }

    // Store new data with otp code into user Object
    async resendOtpCode() {
        const loginUser = await this.apiService.getLoginInfo(this.loginParams());

        if (loginUser.returnStatus.status !== 'SUCCESS') {
            console.log('error condition', loginUser);
            this.notificationsService.alert('Error', loginUser.returnStatus.message);
            return;
        }
        this.loggedInUser = loginUser;
        this.notificationsService.alert('success', 'OTP code has been sent.');
    }

    getTodayDate() {
        const todayDate = new Date();
        const dd = String(todayDate.getDate()).padStart(2, '0');
        const mm = String(todayDate.getMonth() + 1).padStart(2, '0');
        const yyyy = todayDate.getFullYear();

        return dd + '-' + mm + '-' + yyyy ;
    }

    async presentToast(msg, header) {
        const toast = await this.toastController.create({
            message: '<b>' + header + ': </b> ' + msg,
            duration: 2000,
            translucent: true,
            color: header === 'Error' ? 'danger' : 'success'
        });
        toast.present();
    }

}
