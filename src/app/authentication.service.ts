import { Injectable } from '@angular/core';


import { BehaviorSubject, Observable } from 'rxjs';
import { LocalStorageService } from '@app/-common/localStorage.service';
import { ApiService } from '@app/api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(
    private localStorageService: LocalStorageService,
    private apiService: ApiService
  ) { 
    this.currentUserSubject = new BehaviorSubject<any>(this.localStorageService.getItem('help_desk_user'));
    this.currentUser = this.currentUserSubject.asObservable();

    // console.log(this.currentUser);
  }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  async setlLoginUser(user) {

    const userPromise = new Promise((res, rej) => {
      this.localStorageService.setItem('help_desk_user', user);
      this.currentUserSubject.next(user);
      res();
    });

    const requestLov = await this.apiService.getRequestLov();
    this.localStorageService.setItem('REQUEST_LOV', requestLov);

    return userPromise;
    // store user details and jwt token in local storage to keep user logged in between page refreshes
    
    // localStorage.setItem('help_desk_user', JSON.stringify(user));
    
  }

  logout() {
    // remove user from local storage and set current user to null
    const temp = new Promise((res, rej) => {
      localStorage.removeItem('help_desk_user');
      localStorage.clear();
      this.currentUserSubject.next(null);
      res();
    });
    return temp;
  }
}
