import { TestBed } from '@angular/core/testing';

import { DataFlagService } from './data-flag.service';

describe('DataFlagService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataFlagService = TestBed.get(DataFlagService);
    expect(service).toBeTruthy();
  });
});
