import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hrListingFilter'
})
export class HrListingFilterPipe implements PipeTransform {

  transform(hrListing: any, searchStatus: any): any {
    if (!hrListing || hrListing == null || !searchStatus || searchStatus == '' || searchStatus == null ) {
      return hrListing;
    }

    return hrListing.filter((list) => {
     return list.TICKET_STATUS.toLowerCase() == searchStatus.toLowerCase();
    });
  }

}
