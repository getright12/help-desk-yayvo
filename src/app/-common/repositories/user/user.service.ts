import {Injectable} from '@angular/core';

import {Observable, Subscription} from 'rxjs';
import {AppApiService} from '@zm/ng-restful/api.service';
import {AppVars} from '@zm/ng-vars/vars.const';
import {HttpClient} from '@angular/common/http';


@Injectable({
    providedIn: 'root'
})
export class UserService {
    readonly appVars = AppVars;


    constructor(
        private appApiService: AppApiService,
        private http: HttpClient,
    ) {
    }

    login(employeeCode: string, password: string): Observable<any> {
        return this.appApiService.post('RMS.EMPLOYEE.AUTHENTICATION.SETUP', {
            params: {
                employeeAuthenticationRequest: {
                    employeeCode,
                    password
                }
            }
        });
    }

    configValidate(data){
        return this.appApiService.post('RMS.VALIDATE.CONFIG.SETUP', {
            params: {
                validateConfigRequest: {
                    station : data.stationCode,
                    bookingRoute : data.routeCode
                }
            }
        });
    }

    saveConfiguration(data){
        return this.appApiService.post('RMS.CONFIGURATION', {
            params: {
                configurationRequest: {
                    jsonData : data
                }
            }
        });
    }


    logout() {
        const date = new Date();
        // Set it expire in -1 days
        date.setTime(date.getTime() + (-1 * 24 * 60 * 60 * 1000));
        // Set it
        document.cookie = 'rmsVTokenAdmin' + '=; expires=' + date.toUTCString() + '; path=/';
    }
}
