import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';

@Injectable()

export class OfflineStorageService {

    constructor(private storage: Storage) { }

    setData(key, val){
        this.storage.set(key, val);
    }

    getData(key) {
        return this.storage.get(key);
    }
}
