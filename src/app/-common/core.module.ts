import {NgModule, ModuleWithProviders} from '@angular/core';
import {DatePipe} from '@angular/common';

import {AppApiService} from '@zm/ng-restful/api.service';
import {RMSGlobalObjectService} from '@zm/ngwebpack-global-object/rms-global-object.service';
import {OfflineStorageService} from '@app/-common/offlineStorage.service';

import {CookieService} from 'ngx-cookie-service';

@NgModule({})

export class CoreModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreModule,
            providers: [
                AppApiService,
                RMSGlobalObjectService,
                OfflineStorageService,

                CookieService,
                DatePipe
            ]
        };
    }
}
