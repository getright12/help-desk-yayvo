import { Injectable } from '@angular/core';
import {AppApiService} from '@zm/ng-restful/api.service';
import {LocalStorageService} from '@app/-common/localStorage.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private appApiService: AppApiService, private localStorageService: LocalStorageService)
  {
  }
  // api param for getEmployeeAuth
  // tcsEmployeeAuthRequest: {
  //   employeeNumber: this.empNo
  //
  // }

  async getEmployeeAuth(param){
    return await this.appApiService.post('TCS.ONE.EMPLOYEE.AUTH', {
      params: param
    }).toPromise().then(res => res).catch( err => err);
  }
  // api param for getLoginInfo
  // tcsLoginRequest:{
  //   employeeNumber: this.empNo,
  //   applicationId: '1'
  // }
  async getLoginInfo(param){
    return await this.appApiService.post('TCS.ONE.LOGIN', {
        params: param
      }).toPromise().then(res => res).catch( err => err);
  }
  // api param for getLogRequestDetails
  // "tcsLogRequestDetails":{
  //   "employeeID":"119333",
  //   "fromDate":"20200723",
  //   "toDate":"20200806",
  //   "applicationID":"1"
  // }
  async getLogRequestDetails(param){
    return await this.appApiService.post('TCS.ONE.LOG.REQUEST.DETAILS', {
      params: param
    }).toPromise().then(res => res).catch( err => err);
  }

  async getRequestLov()
  {
    return await this.appApiService.post('TCS.ONE.QUERY.STATUS', {
      params: {
      }
    }).toPromise().then(res => res).catch( err => err);
  }
  // api param for getInsertRequest
  // "tcsLogRequest": {
  //   "employeeID": 111933,
  //   "requesterEmployeeID": 11933,
  //   "requestModeID": 4,
  //   "matrixID": 169,
  //   "masterQueryID": 11,
  //   "detailQueryID": 7,
  //   "requesterRemarks": "test",
  //   "hrRemarks": ""
  // }
  async getInsertRequest(param)
  {
    return await this.appApiService.post('TCS.ONE.LOG.REQUEST', {
      params: param
    }).toPromise().then(res => res).catch( err => err);
  }

  // api param for updateRequestLog
  // "tcsUpdateLogRequest": {
  //   "ticketID": 101,
  //   "ticketStatus": "Rejected",
  //   "hrRemarks": "Input HR Remarks",
  //   "updatedByEmployeeID": "6400"
  // }
  async updateRequestLog(param){
    return await this.appApiService.post('TCS.ONE.UPDATE.LOG.REQUEST', {
      params: param
    }).toPromise().then(res => res).catch( err => err);
  }
  // api param for getHrGraphDetail
  // "tcsAllRequestGraph": {
  //   "employeeID": "110527",
  //   "applicationID": "1",
  //   "fromDate": "20200430",
  //   "toDate": "20200730",
  //   "mqID": "1",
  //   "dqID": ""
  // }
  async getHrGraphDetail(param){
    return await this.appApiService.post('TCS.ONE.ALL.REQUEST.GRAPH', {
      params: param
    }).toPromise().then(res => res).catch( err => err);
  }
  async getIssuesLov(){
    return await this.appApiService.post('TCS.ONE.QUERY.ISSUES.LOV', {
      params: {

      }
    }).toPromise().then(res => res).catch( err => err);
  }
  // api param for getSubQueryDetailsAgainstMasterId
  // "requestGraphSubTitle": {
  //   "employeeID": "115902",
  //   "applicationID": "1",
  //   "fromDate": "20200625",
  //   "toDate": "20200703",
  //   "mqID": "3",
  //   "dqID": ""
  // }
  async getSubQueryDetailsAgainstMasterId(param){
    return await this.appApiService.post('TCS.ONE.REQUEST.SUB.GRAPH', {
      params: param
    }).toPromise().then(res => res).catch( err => err);
  }
  // api param for getNotificationList
  // "appNotificationRequest":{
  //   "employeeNumber":"109821",
  //   "requestType":""
  // }
  async getNotificationList(param){
    return await this.appApiService.post('TCS.ONE.APP.NOTIFICATIONS.LOV', {
      params: param
    }).toPromise().then(res => res).catch( err => err);
  }
  // api param for updateAppToken
  // "updateAppTokenRequest": {
  //   "employeeID": 115902,
  //   "appToken": "InputToken"
  //
  // }
  async updateAppToken(param){
    return await this.appApiService.post('TCS.ONE.UPDATE.APP.TOKEN', {
      params: param
    }).toPromise().then(res => res).catch( err => err);
  }
  // api param for getUserQueryDetails
  // "getTicketInfoNotify":{
  //   "ticketID": 333,
  //   "applicationID":"1"
  // }
  async getUserQueryDetails(param){
    return await this.appApiService.post('TCS.ONE.GET.TICKET.NOTIFY', {
      params: param
    }).toPromise().then(res => res).catch( err => err);
  }
  //   api param for getDashboardGraph
  //   "dashboardGraphHome":{
  //     "jsonData":{
  //       "batches":[
  //         {
  //           "GraphName":"HR-GRAPSH1",
  //           "sqid":1
  //         },
  //         {
  //           "GraphName":"HR-GRAPSH2",
  //           "sqid":2
  //         }
  //
  //       ]
  //     },
  //     "applicationType":1,
  //     "moduleID":1,
  //     "fromDate":"20200301",
  //     "toDate":"20200715",
  //     "employeeID":110527
  //   }
  // }
  async getDashboardGraph(param){
    return await this.appApiService.post('TCS.ONE.DASHBOARD.GRAPH.HOME', {
      params: param
    }).toPromise().then(res => res).catch( err => err);
  }
  // api param for getRemarkList
  // "remarksListRequest":{
  //   "mqID":"1",
  //   "dqID":"1",
  //   "moduleID":1
  // }
  async getRemarkList(param){
    return await this.appApiService.post('TCS.ONE.REMARKS.LIST', {
      params: param
    }).toPromise().then(res => res).catch( err => err);
  }

  getEmployeeId(){
    return this.localStorageService.getItem('help_desk_user').employeeId;
  }
}
