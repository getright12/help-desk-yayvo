import { Injectable, Component , HostListener} from '@angular/core';
import {ModalController, ToastController, AlertController} from '@ionic/angular';




@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor(private modalController: ModalController, private alertController: AlertController,
              private toastController: ToastController) { }

    async toast(msg, header = 'success') {
      const toast = await this.toastController.create({
          message: msg,
          duration: 2000,
          color: header === 'Error' ? 'danger' : 'success'
      });
      toast.present();
    }

    async alert(header, msg) {
      const alert = await this.alertController.create({
          header,
          message: msg,
          buttons: ['OK']
      });
      await alert.present();
  }
}
