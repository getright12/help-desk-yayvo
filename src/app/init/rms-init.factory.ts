import {CookieService} from 'ngx-cookie-service';

import {AppApiService} from '@zm/ng-restful/api.service';
import {RMSGlobalObjectService} from '@zm/ngwebpack-global-object/rms-global-object.service';
import {OfflineStorageService} from '@app/-common/offlineStorage.service';
import {AppVars} from '@zm/ng-vars/vars.const';
import {LocalStorageService} from '@app/-common/localStorage.service';
import {UserService} from '@app/-common/repositories/user/user.service';
import {stringify} from 'querystring';


export function rmsInitFactory(
    cookieService: CookieService,
    appApiService: AppApiService,
    rmsGlobalObjectService: RMSGlobalObjectService,
    offlineStorageService: OfflineStorageService,
    localStorageService: LocalStorageService,
) {
    return (val: any) => {
        console.log('RMSInitFactory');

        // const empNo = '109821';
        // const getThisUser = new Promise((resolve, reject) => {
        //     offlineStorageService.getData('HELP_DESK_EMP_AUTH').then((data) => {
        //
        //         if (data) {
        //
        //             resolve();
        //
        //         } else {
        //             appApiService.post('TCS.ONE.EMPLOYEE.AUTH', {
        //                 params: {
        //                     tcsEmployeeAuthRequest:{
        //
        //                         employeeNumber:empNo
        //
        //                     }
        //                 }
        //             }).subscribe((res) => {
        //                 console.log('Emp Auth :: ',res);
        //                 resolve();
        //
        //             }, (err) => {
        //                 console.log('err', err);
        //             });
        //         }
        //
        //
        //     });
        // });
        //
        // const getLoginInfo = new Promise((resolve, reject) => {
        //     offlineStorageService.getData('HELP_DESK_LOGIN').then((data) => {
        //         if (data) {
        //
        //             resolve();
        //
        //         } else {
        //             appApiService.post('TCS.ONE.LOGIN', {
        //                 params: {
        //                     tcsLoginRequest:{
        //                         employeeNumber:empNo,
        //                         applicationId:"1"
        //                     }
        //                 }
        //             }).subscribe((res) => {
        //                 console.log('Login Info :: ',res);
        //                 resolve();
        //
        //             }, (err) => {
        //                 console.log('err', err);
        //             });
        //         }
        //
        //
        //     });
        // });
        // const getQueryStatus = new Promise((resolve, reject) => {
        //     offlineStorageService.getData('HELP_DESK_QUERY_STATUS').then((data) => {
        //         if (data) {
        //
        //             resolve();
        //
        //         } else {
        //             appApiService.post('TCS.ONE.QUERY.STATUS', {
        //                 params: {
        //                 }
        //             }).subscribe((res) => {
        //                 console.log('query status :: ',res);
        //                 resolve();
        //
        //             }, (err) => {
        //                 console.log('err', err);
        //             });
        //         }
        //
        //
        //     });
        // });
        // const getLogRequestDetails = new Promise((resolve, reject) => {
        //     offlineStorageService.getData('HELP_DESK_LOG_REQUEST_DETAILS').then((data) => {
        //         const empNo = '109821';
        //
        //         if (data) {
        //
        //             resolve();
        //
        //         } else {
        //             appApiService.post('TCS.ONE.LOG.REQUEST.DETAILS', {
        //                 params: {
        //                     tcsLogRequestDetails:{
        //                         employeeID:empNo,
        //                         "fromDate":"20200723",
        //                         "toDate":"20200806",
        //                         "applicationID":"1"
        //                     }
        //                 }
        //             }).subscribe((res) => {
        //                 console.log('query status :: ',res);
        //                 resolve();
        //
        //             }, (err) => {
        //                 console.log('err', err);
        //             });
        //         }
        //
        //
        //     });
        // });
        //
        // const getRequestLov = new Promise((resolve, reject) => {
        //     offlineStorageService.getData('HELP_DESK_REQUEST_LOV').then((data) => {
        //         const empNo = '109821';
        //
        //         if (data) {
        //
        //             resolve();
        //
        //         } else {
        //             appApiService.post('TCS.ONE.QUERY.STATUS', {
        //                 params: {
        //                 }
        //             }).subscribe((res) => {
        //                 console.log('request  lov :: ',res);
        //                 resolve();
        //
        //             }, (err) => {
        //                 console.log('err', err);
        //             });
        //         }
        //
        //
        //     });
        // });

        /*return Promise.all([
            getThisUser,

        ]);*/

    };
}
