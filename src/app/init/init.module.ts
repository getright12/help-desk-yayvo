import {NgModule, APP_INITIALIZER, ModuleWithProviders} from '@angular/core';

import {CookieService} from 'ngx-cookie-service';
import {AppApiService} from '@zm/ng-restful/api.service';
import {LocalStorageService} from '@app/-common/localStorage.service';
import {RMSGlobalObjectService} from '@zm/ngwebpack-global-object/rms-global-object.service';
import {OfflineStorageService} from '@app/-common/offlineStorage.service';
import {rmsInitFactory} from './rms-init.factory';

export function rmsSiteInit() {
    return () => {
    };
}


@NgModule({
    providers: [{
        provide: APP_INITIALIZER,
        useFactory: rmsSiteInit,
        deps: [
            RMSGlobalObjectService,
            OfflineStorageService
        ],
        multi: true
    }, {
        provide: APP_INITIALIZER,
        useFactory: rmsInitFactory,
        deps: [
            CookieService,
            AppApiService,
            RMSGlobalObjectService,
            OfflineStorageService,
            LocalStorageService,
        ],
        multi: true
    }]
})

export class InitModule {
}
