import { Component, OnInit, ViewChild } from '@angular/core';
import {ModalController, AlertController, Animation, AnimationController } from '@ionic/angular';
import { Chart } from 'chart.js';
import {ApiService} from '@app/api.service';
import * as moment from 'moment';
import { LoadingController } from '@ionic/angular';
import {LocalStorageService} from "@app/-common/localStorage.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  public graphChartDataAvailable = false;
	public graphChartData: any;
  public dashboardGraph_from: any = moment(new Date()).subtract(30, "days").format("YYYYMMDD");
  public dashboardGraph_to: any = moment(new Date()).format("YYYYMMDD");

  constructor(private apiService: ApiService, public loadingController: LoadingController, public alertController: AlertController,
              private localStorageService: LocalStorageService, private router: Router) { }

  ionViewDidEnter(){
  	this.apiCallGetDashboardGraph();
  }

  ngOnInit(){
    // document.querySelector('.main-loader').style.display = 'block';
  }

  async apiCallGetDashboardGraph() {
    this.presentLoading();
  	const req_params = {
  		"dashboardGraphHome":{
  		   "jsonData":{
  		     "batches":[
  		                            {
  		            "GraphName":"HR-GRAPSH1",
  		            "sqid":1
  		        },
  		         {
  		            "GraphName":"HR-GRAPSH2",
  		            "sqid":2
  		        }

  		      ]
  		   },
  		   "applicationType":1,
  		   "moduleID":1,
  		   "fromDate": this.dashboardGraph_from,
  		   "toDate": this.dashboardGraph_to,
  		   "employeeID":this.apiService.getEmployeeId()
  		}
  	}
    const dashboard_data = await this.apiService.getDashboardGraph(req_params);
    
    if(dashboard_data.returnStatus.status != 'SUCCESS'){
      this.graphChartDataAvailable = false;
      this.dismissLoading();
      this.presentAlert('Error', dashboard_data.returnStatus.message);
      return;
    }
    
    if('graphs' in dashboard_data && dashboard_data.graphs.length > 0){
      this.graphChartDataAvailable = true;
      this.graphChartData = dashboard_data.graphs[0].data[0];
      this.showChart();
    }
    
    this.dismissLoading();
  }

  showChart() {
  	let canvas_element_old = document.querySelector('#dashboard_chart');
    if(canvas_element_old)
      canvas_element_old.remove();

    let canvas_element_new = document.createElement("Canvas");
    canvas_element_new.setAttribute("id", "dashboard_chart");
    
    if(document.querySelector('#dashboard_chart_container'))
      document.querySelector('#dashboard_chart_container').appendChild(canvas_element_new);

  	var ctx = (<any>document.getElementById('dashboard_chart')).getContext('2d');
    var chart = new Chart(ctx, {
        type: 'doughnut',
        data: {
	        labels: [
	        'Approved: '+ this.graphChartData.APPROVED_TOTAL,
	        'Rejected: '+ this.graphChartData.REJECTED_TOTAL,
	        'Pending: '+ this.graphChartData.PENDING_TOTAL],
	        datasets: [{
	              label: "Dashabord One Month Data",
	              backgroundColor: [
	                'green',
	                'red',
	                'orange',
	              ],
	              data: [this.graphChartData.APPROVED_TOTAL, this.graphChartData.REJECTED_TOTAL, this.graphChartData.PENDING_TOTAL]
	        }]
       },
       options: {
          legend: {
              display: true,
              labels: {
                  fontColor: '#3d3d3d'
              },
              position: 'left'
          },
          legendCallback: function(chart) {
              // Return the HTML string here.
          },
          animation: {
            duration: 0 // general animation time
          },
      	}
    });
  }

  handleManagerChart(){
    const role = this.localStorageService.getItem('help_desk_user').loginEmployeeProfile[0].ROLE_ASSIGNED;

    if(role === 'N/A')
        return;

    this.router.navigate(['/manager-dashboard']);
  }

  async presentAlert(header, message) {
    const alert = await this.alertController.create({
      cssClass: 'help_desk_popup',
      header: header,
      message: message,
      buttons: [{
        text: 'Ok',
        handler: () => {
          // console.log('Cancel clicked');
        }
      }/*,
        {
          text: 'Confirm',
          handler: () => {
            console.log('Buy clicked');
          }
        }*/]
    });

    await alert.present();
  }

  async presentLoading() {
    /*this.animationCtrl.create()
    .addElement(document.querySelector('.component_loader'))
    .duration(200)
    .fromTo('left', '0', '0')
    .fromTo('opacity', '1', '1').play()*/

    const loading = await this.loadingController.create({
      cssClass: 'c_loader',
      message: 'Please wait...',
    });
    await loading.present();
  }

  dismissLoading() {
    /*this.animationCtrl.create()
    .addElement(document.querySelector('.component_loader'))
    .duration(200)
    .fromTo('left', '0', '100%')
    .fromTo('opacity', '1', '1').play()*/
    setTimeout(()=>{
      this.loadingController.dismiss(null, undefined);
    }, 500)
  }
}
