import { Component, OnInit } from '@angular/core';
import { ApiService } from '@app/api.service';
import {LocalStorageService} from '@app/-common/localStorage.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalController, AlertController} from '@ionic/angular';
import {Router, ActivatedRoute} from '@angular/router';
import {NotificationsService} from '@app/notifications.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-new-request',
  templateUrl: './new-request.page.html',
  styleUrls: ['./new-request.page.scss'],
})
export class NewRequestPage implements OnInit {

  requestLov: any;
  requestMode = [];
  remarkList: any;
  queryMaster = [];
  masterId: number;
  queryMatrix: any;
  currentReqModeTxt: any;
  queryDetailsArray = [];
  region: any;
  isAgentStatus = true;
  newRequestForm: FormGroup;
  isPointOfContact = false;
  isEmployeeIdShow = true;
  isEmployeeNameShow = false;

  constructor(private apiService: ApiService,
              private formBuilder: FormBuilder,
              private alertController: AlertController,
              private router: Router,
              private notificationsService: NotificationsService,
              private localStorageService: LocalStorageService,
              public loadingController: LoadingController) { }

  ngOnInit(){
    this.initRequestForm();
    this.requestLov  = this.getRequestType();
    this.region = this.getRegion();
    this.isAgentStatus = this.getAgentStatus();

    if(!this.isAgentStatus){
      this.newRequestForm.patchValue({employeeId: this.getEmployeeId()});
      this.newRequestForm.patchValue({requestType: 4});
      this.newRequestForm.get('employeeId').disable();
      this.newRequestForm.get('employeeId').setValidators(null);
      this.getEmployeeDetails();
      this.isEmployeeNameShow = true;
    }
    if(this.requestLov) {
      this.requestMode = this.requestLov.requestModes;
      this.queryMaster = this.requestLov.queryMaster;
    }
  }

  initRequestForm() {
    this.newRequestForm = this.formBuilder.group({

      requestType: ['', Validators.required ],
      employeeName: '',
      hrRemarks: '',
      employeeId: ['', Validators.required ],
      masterQuery: ['', Validators.required ],
      subQuery: ['', Validators.required ],
      remarks: ['', Validators.required ],

    });
  }

  getPayload(employee, requesterEmployeeID){
    return {
      tcsLogRequest:{
        employeeID: employee,
        requesterEmployeeID: requesterEmployeeID,
        requestModeID: this.newRequestForm.get('requestType').value,
        matrixID: this.queryMatrix[0].MAT_ID,
        masterQueryID: this.newRequestForm.get('masterQuery').value,
        detailQueryID: this.newRequestForm.get('subQuery').value,
        requesterRemarks: this.newRequestForm.get('remarks').value,
        hrRemarks: ""
      }
    };
  }

  async onFormSubmit()
  {
    this.presentLoading();
    this.newRequestForm.enable();
    const requestId = this.newRequestForm.get('requestType').value;
    let employee = this.getEmployeeId();
    let requesterEmployeeID = this.newRequestForm.get('employeeId').value;

    if(requestId == 4){
      requesterEmployeeID = employee;
    }

    const payload = this.getPayload(employee, requesterEmployeeID);
    const response = await this.apiService.getInsertRequest(payload);

    if(response.returnStatus.status !== 'SUCCESS') {
      this.presentAlert('Error', 'Api is not responding .');
      return;
    }
    this.refreshForm();
    this.dismissLoading();
    this.notificationsService.toast('Request has been placed successfully.');
    setTimeout(() => {this.router.navigate(['/hr-listing'])},1000);
  }

  getRegion(){
    return this.localStorageService.getItem('help_desk_user').loginEmployeeProfile[0].EMP_REGION;
  }
  
  getEmployeeId(){
    return this.localStorageService.getItem('help_desk_user').employeeId;
  }
  
  getEmployeeName(){
    return this.localStorageService.getItem('help_desk_user').loginEmployeeProfile[0].EMP_NAME;
  }
  
  getAgentStatus(){
    const status = this.localStorageService.getItem('help_desk_user').loginEmployeeProfile[0].AGENT_STATUS;

    if(status === 'Y') {
      return true;
    }
    else{
      return false;
    }
  }
  
  getRequestType(){
    const request = this.localStorageService.getItem('REQUEST_LOV');

    if(request.returnStatus.status !== 'SUCCESS') {
      this.presentAlert('Error', 'Api is not responding .');
      return;
    }
    return request;
  }

  getQueryDetail(masterId){

    const queryDetails = this.requestLov.queryDetail;
    this.queryDetailsArray = [];

    const keys = Object.keys(queryDetails);
    for (const key of keys) {
      const id = queryDetails[key].DQ_ID;
      if(queryDetails[key].MQ_ID === masterId) {
        this.queryDetailsArray.push(queryDetails[key]);
      }
    }

    return this.queryDetailsArray;
  }

  async getEmployeeDetails()
  {
    const empId = this.newRequestForm.get('employeeId').value;
    if(this.isNull(empId))
    {
      this.newRequestForm.patchValue({employeeName: ''});
      this.isEmployeeNameShow = false;
      this.presentAlert('Error', 'Please provide employee id.');
      return;
    }

    // this.presentLoading();
    await this.getEmployeefromApi(empId);
    this.isEmployeeNameShow = true;
    // this.dismissLoading();
  }

  async getEmployeefromApi(empId){
    const empDetails = await this.apiService.getEmployeeAuth(this.getEmployeeParam(empId));
    if(empDetails.returnStatus.status !== 'SUCCESS')
    {
      this.presentAlert('Error', 'Api is not responding .');
      return;
    }
    const empName = empDetails.employeeAuthProfile[0].EMP_NAME;
    this.newRequestForm.patchValue({employeeName: empName});
  }

  getEmployeeParam(id){
      return {
          tcsEmployeeAuthRequest: {
              employeeNumber: id
          }
      };
  }

  getQueryMatrix(masterId, queryDetailId, region)
  {
    const queryMatrix = this.requestLov.queryMatrix;
    const queryMatrixWithOutRegion = [];
    const queryMatrixWithRegion = [];

    const q_mat = queryMatrix.filter((i,v)=>{
      if(i.MQ_ID === masterId && i.DQ_ID === queryDetailId && i.REGION === region){
        queryMatrixWithRegion.push(i);
      }
      else if(i.MQ_ID === masterId && i.DQ_ID === queryDetailId){
        queryMatrixWithOutRegion.push(i);
      }
    })

    if(!this.isNull(region) && queryMatrixWithRegion.length > 0)
    {
      return [queryMatrixWithRegion[0]];
    }
    else{
      return [queryMatrixWithOutRegion[0]];
    }

  }

  queryMasterChange(event)
  {
    this.masterId = parseInt(event.target.value, 10);
    this.getQueryDetail(this.masterId);

    this.newRequestForm.patchValue({remarks: ''});
    this.newRequestForm.patchValue({subQuery: ''});
  }

  async queryDetailChange(event)
  {
    const id = parseInt(event.target.value, 10);
    this.queryMatrix = this.getQueryMatrix(this.masterId, id, this.region);
    this.isPointOfContact = true;

    if(id && this.masterId)
    {
        this.remarkList = await this.apiService.getRemarkList(this.getRemarksParam(id));
        if(this.remarkList.returnStatus.status !== 'SUCCESS')
        {
            this.presentAlert('Error', 'Api is not responding .');
            return;
        }
        this.remarkList = this.remarkList.userRemarks;
    }
  }

  getRemarksParam(detailId){
      const mqId = this.masterId + '';
      const dqId = detailId + '';
      return  {
          remarksListRequest: {
              mqID: mqId,
              dqID: dqId,
              moduleID: 1
          }
      };
  }

  async requestModeChange(event){
    const id = parseInt(event.target.value, 10);
    this.isPointOfContact = false;
    this.isEmployeeNameShow = false;

    this.refreshFormWithOutRequestMode();
    this.newRequestForm.get('employeeId').enable();
    this.newRequestForm.get('employeeName').enable();

    if(id === 4)
    {
      this.newRequestForm.patchValue({employeeId: this.getEmployeeId()});
      this.newRequestForm.get('employeeId').disable();
      this.newRequestForm.get('employeeId').setValidators(null);


      this.isEmployeeNameShow = true;
      this.newRequestForm.patchValue({employeeName: this.getEmployeeName()});
      this.newRequestForm.get('employeeName').disable();
    } else{
      this.newRequestForm.get('employeeId').setValidators([Validators.required]);
      this.isEmployeeIdShow = true;
    }
  }
  refreshFormWithOutRequestMode(){
    this.newRequestForm.patchValue({
      employeeId: '',
      // employeeName: '',
      hrRemarks: '',
      masterQuery: '',
      subQuery: '',
      remarks: '',

    });
  }

  refreshForm()
  {
    this.isEmployeeIdShow = true;
    this.isEmployeeNameShow = false;
    this.isPointOfContact = false;
    this.newRequestForm.patchValue({
      requestType: '',
      employeeId: '',
      employeeName: '',
      hrRemarks: '',
      masterQuery: '',
      subQuery: '',
      remarks: '',

    });
  }

  isNull(value) {
    return value === null || value === '' || typeof value === 'undefined' || value.toString().length < 1;
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  async presentAlert(header, msg) {
    const alert = await this.alertController.create({
      header,
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'c_loader',
      message: 'Please wait...',
    });
    await loading.present();
  }

  dismissLoading() {
    setTimeout(()=>{
      this.loadingController.dismiss(null, undefined);
    }, 500)
  }
}
