import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HrListingPageRoutingModule } from './hr-listing-routing.module';
import {HrListingFilterPipe} from '@app/hr-listing-filter.pipe';

import { HrListingPage } from './hr-listing.page';
import {SharedModule} from '@app/-common/shared.module';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    IonicModule,
    HrListingPageRoutingModule
  ],
  declarations: [HrListingPage]
})
export class HrListingPageModule {}
