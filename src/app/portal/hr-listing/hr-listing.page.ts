import { Component, OnInit, ElementRef } from '@angular/core';
import {ModalController, AlertController, Animation, AnimationController } from '@ionic/angular';
import { Chart } from 'chart.js';
import * as moment from 'moment';
import {ApiService} from '@app/api.service';
import {Router, ActivatedRoute} from '@angular/router';
import {DataFlagService} from '@app/data-flag.service';
import { LoadingController } from '@ionic/angular';
import {LocalStorageService} from '@app/-common/localStorage.service';

@Component({
  selector: 'app-hr-listing',
  templateUrl: './hr-listing.page.html',
  styleUrls: ['./hr-listing.page.scss'],
})
export class HrListingPage implements OnInit {

	public request_data: any = false;
  public respond_data: any = false;
	public request_graph_data: any = false;
  public graph_from: any = moment(new Date()).subtract(15, "days").format("YYYY-MM-DD");
  public graph_to: any = moment(new Date()).format("YYYY-MM-DD");
  public total_approved = '';
  public total_rejected = '';
  public total_pending = '';
  public date_wise_data = {};
  filterListingByStatus: any; 
  togglePending: boolean = false;
  toggleApproved: boolean = false;
  toggleRejected: boolean = false;
  employeeRole: any;

  constructor(
    private apiService: ApiService,
    private router: Router,
    private dataFlagService: DataFlagService,
    public alertController: AlertController,
    public loadingController: LoadingController,
    private localStorageService: LocalStorageService,
    private animationCtrl: AnimationController,
    ){
      this.employeeRole = this.localStorageService.getItem('help_desk_user').loginEmployeeProfile[0].ROLE_ASSIGNED;
    /*this._tempArr = this._tempObj.map((i,v)=>{
      // console.log(i.TICKET_DATE).format('YYYY-MM-DD')
      return moment(i.TICKET_DATE).format('YYYY-MM-DD');
    })*/

    // this._tempArr = [...new Set(this._tempArr)];

    /*this._tempArr.forEach((i,v)=>{
      
      let temp_key_date = i;
      // console.log(i);

      let date_data = [];

      this._tempObj.filter((i2,v2)=>{
        if(moment(i2.TICKET_DATE).format('YYYY-MM-DD')==temp_key_date){
          // console.log(moment(i2.TICKET_DATE).format('YYYY-MM-DD'), temp_key_date)
          // return {
          //   'pending': i2.TICKET_STATUS.toLowerCase()=='pending' ? 1 : 0,
          //   'approved': i2.TICKET_STATUS.toLowerCase()=='approved' ? 1 : 0,
          //   'rejected': i2.TICKET_STATUS.toLowerCase()=='rejected' ? 1 : 0,
          // };

          date_data.push({
            'pending': i2.TICKET_STATUS.toLowerCase()=='pending' ? 1 : 0,
            'approved': i2.TICKET_STATUS.toLowerCase()=='approved' ? 1 : 0,
            'rejected': i2.TICKET_STATUS.toLowerCase()=='rejected' ? 1 : 0,
          })
        }
      })

      this.date_wise_data[temp_key_date] = date_data;
    })*/

  }

  ngOnInit() {
    
  }

  ionViewDidEnter(){
    this.apiCallgetLogRequestDetails();
  }

  async apiCallgetLogRequestDetails() {
    this.presentLoading();
  	const req_params = {
  		"tcsLogRequestDetails":{
           "employeeID":this.apiService.getEmployeeId(),
           "fromDate":moment(this.graph_from).format("YYYYMMDD"),
           "toDate":moment(this.graph_to).format("YYYYMMDD"),
           "applicationID":"1"
        }
  	}
    const logs_data = await this.apiService.getLogRequestDetails(req_params);
    if (logs_data.returnStatus.status != 'SUCCESS') {
        this.dismissLoading();
        this.presentAlert('Error', logs_data.returnStatus.message);
        return;
    }

    if(logs_data['logRequestDetails']){
    	this.request_data = logs_data.logRequestDetails;
  		// console.log(this.request_data, 'request');
    }
    else{
      this.request_data = '';
    }

    if(logs_data['responder']) {
      this.respond_data = true;
      // this.dataFlagService.setResponderData(logs_data.responder);
      this.localStorageService.setItem('hr_responder', logs_data.responder);

      // this.localStorageService.setItem('hr_responder', {'listing': logs_data.responder, 'graphs': logs_data.graphSummary2});
    }
    
    if(logs_data['graphSummary1']){
      // console.log(logs_data.graphSummary1);
      this.request_graph_data = logs_data.graphSummary1;
      this.renderGraphs();
    }
    else{
      this.request_graph_data = [];
      this.renderGraphs();
    }
    // console.log(this.request_graph_data);
    this.dismissLoading();
  }

  showQueryDetail = (id, isResponder) => {
    // Passing Responder check
    this.dataFlagService.changeResponderFlag(isResponder);
    this.router.navigate(['/hr-query-detail', id]);
  }

  toggle_requests = (type: string) => {
    if (type.toLowerCase() === 'pending') {
      this.togglePending = !this.togglePending;
      this.toggleRejected = false;
      this.toggleApproved = false;
    }

    if (type.toLowerCase() === 'approved') {
      this.toggleApproved = !this.toggleApproved;
      this.toggleRejected = false;
      this.togglePending = false;
    }
    
    if (type.toLowerCase() === 'rejected') {
      this.toggleRejected = !this.toggleRejected;
      this.toggleApproved = false;
      this.togglePending = false;
    }

    if (this.togglePending || this.toggleApproved || this.toggleRejected) {
      this.filterListingByStatus = type;
    } else {
      this.filterListingByStatus = '';
    }
  }

  renderGraphs() {
    
    let canvas_element_old = document.querySelector('#listing_charts');
    if(canvas_element_old)
      canvas_element_old.remove();

    let canvas_element_new = document.createElement("Canvas");
    canvas_element_new.setAttribute("id", "listing_charts");
    
    if(document.querySelector('#listing_charts_container'))
      document.querySelector('#listing_charts_container').appendChild(canvas_element_new);

    let chartData = this.request_graph_data;
    let date_labels = [];
    let date_approved = [];
    let date_rejected = [];
    let date_pending = [];

    chartData.forEach((i,v)=>{
       date_labels.push(moment(i.TICKET_DATE).format("MMM-DD"));
       date_approved.push(i.APPROVED_TOTAL);
       date_rejected.push(i.REJECTED_TOTAL);
       date_pending.push(i.PENDING_TOTAL);
    })

    var barChartData = {
			labels: date_labels,
			datasets: [
      {
				label: 'Approved',
				backgroundColor: 'green',
				stack: 'Stack 0',
				data: date_approved
			},
      {
				label: 'Rejected',
				backgroundColor: 'red',
				stack: 'Stack 0',
				data: date_rejected
			},
      {
				label: 'Pending',
				backgroundColor: 'orange',
				stack: 'Stack 0',
				data: date_pending
			}]
		};

    var ctx = (<any>document.getElementById('listing_charts')).getContext('2d');

    var chart = new Chart(ctx, {
      type: 'bar',
      data: barChartData,
      options: {
        legend: {
          display: true,
          labels: {
            fontColor: '#3d3d3d'
          },
          position: 'bottom',
          scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true                
						}]
					}
        },
        animation: {
          duration: 0 // general animation time
        },
    	}
    });

    // get total approved
    this.total_approved = date_approved.reduce((total, date_approved)=> {
      return total + date_approved;
    }, 0);

    // get total rejected
    this.total_rejected = date_rejected.reduce((total, date_rejected)=> {
      return total + date_rejected;
    }, 0);

    // get total pending
    this.total_pending = date_pending.reduce((total, date_pending)=> {
      return total + date_pending;
    }, 0);
  }

  handleSelectData = (event, type)=>{
    let temp = event.detail.value;

    if(type=='from'){
      this.graph_from = moment(temp).format("YYYYMMDD");
    }
    else if(type=='to'){
      this.graph_to = moment(temp).format("YYYYMMDD");
    }

    // var date = new Date(temp);
    // console.log(date.getMonth());
    // console.log(moment(temp).format("YYYYMMDD"));
    // console.log(moment(new Date(temp)).subtract(15, "days").format("YYYYMMDD"))
  }


  handleFilterRequest = ()=>{
    /*console.log('from', this.graph_from);
    console.log('to', this.graph_to);*/
    this.filterListingByStatus = '';// reset listing filter 
    this.toggleApproved = false;
    this.toggleRejected = false;
    this.togglePending = false;
    
    if(moment(this.graph_from).format("YYYYMMDD") > moment(this.graph_to).format("YYYYMMDD")){
      this.presentAlert('Alert', `From Date cant be greater than the To Date`);
    }
    else{
      this.apiCallgetLogRequestDetails();
    }
  }

  async presentAlert(header, message) {
    const alert = await this.alertController.create({
      cssClass: 'help_desk_popup',
      header: header,
      message: message,
      buttons: [{
        text: 'Ok',
        handler: () => {
          // console.log('Cancel clicked');
        }
      }/*,
        {
          text: 'Confirm',
          handler: () => {
            console.log('Buy clicked');
          }
        }*/]
    });

    await alert.present();
  }

  async presentLoading() {
    /*this.animationCtrl.create()
    .addElement(document.querySelector('.component_loader'))
    .duration(200)
    .fromTo('left', '0', '0')
    .fromTo('opacity', '1', '1').play()*/

    const loading = await this.loadingController.create({
      cssClass: 'c_loader',
      message: 'Please wait...',
    });
    await loading.present();
  }

  dismissLoading() {
    /*this.animationCtrl.create()
    .addElement(document.querySelector('.component_loader'))
    .duration(200)
    .fromTo('left', '0', '100%')
    .fromTo('opacity', '1', '1').play()*/
    setTimeout(()=>{
      this.loadingController.dismiss(null, undefined);
    }, 500)
  }
}