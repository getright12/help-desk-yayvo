import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HrListingPage } from './hr-listing.page';

describe('HrListingPage', () => {
  let component: HrListingPage;
  let fixture: ComponentFixture<HrListingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HrListingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HrListingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
