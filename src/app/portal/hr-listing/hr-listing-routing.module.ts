import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HrListingPage } from './hr-listing.page';

const routes: Routes = [
  {
    path: '',
    component: HrListingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HrListingPageRoutingModule {}
