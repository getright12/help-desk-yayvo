import { Component, OnInit } from '@angular/core';
import {ApiService} from '@app/api.service';
import * as moment from 'moment';
import { Chart } from 'chart.js';
import {ModalController, AlertController, LoadingController} from '@ionic/angular';
import {ManagerDashboardModalPage} from '@app/portal/manager-dashboard-modal/manager-dashboard-modal.page';
import {LocalStorageService} from '@app/-common/localStorage.service';
import {main} from "@angular/compiler-cli/src/main";
import { NgCircleProgressModule } from 'ng-circle-progress';

@Component({
  selector: 'app-manager-dashboard',
  templateUrl: './manager-dashboard.page.html',
  styleUrls: ['./manager-dashboard.page.scss'],
})
export class ManagerDashboardPage implements OnInit {

  public threeMonth  = moment().subtract(3, 'M').format('YYYYMMDD');
  public currentMonth = moment(new Date()).format('YYYYMMDD');

  masterDataAvailiable = false;
  filterDataAvailiable = false;
  hrGraphData:any;
  threeMonthsGraph = [];
  graphTAT = [];
  graphMasterRequest = [];
  tempGraphMasterRequest = [];
  response = false;
  filterName = '';

  threeMonthOptions = {
    initialSlide: 0,
    loop: false,
    slidesPerView: 3,
    spaceBetween: 0
  };

  graphTatOptions = {
    initialSlide: 0,
    loop: false,
    slidesPerView: 3,
    spaceBetween: 0
  };

  mySlideOptions = {
    initialSlide: 0,
    loop: false,
    freeMode: true,
    slidesPerView: 'auto',
    spaceBetweenSlides: 10,
    /*breakpoints: {
      767: {
        slidesPerView: 3,
        spaceBetweenSlides: 10
      }
    }*/
  };

  constructor(private apiService: ApiService, private modalController: ModalController,
              private localStorageService: LocalStorageService, public loadingController: LoadingController) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.apiCallGetDashboardGraph();
  }

  getParam(){
    return {
      tcsAllRequestGraph: {
        employeeID: "117480",
        applicationID: "1",
        fromDate: "20200601",
        toDate: "20200830",
        mqID: "",
        dqID: ""
      }
    }
  }

  async apiCallGetDashboardGraph() {

    this.hrGraphData = await this.apiService.getHrGraphDetail(this.getParam());

    if (this.hrGraphData.returnStatus.status !== 'SUCCESS'){
      this.masterDataAvailiable = true;
      return;
    }
    this.masterDataAvailiable = false;
    this.threeMonthsGraph = this.hrGraphData.graphThreeMonths;
    this.graphTAT = this.hrGraphData.graphTAT;
    this.graphMasterRequest = this.hrGraphData.graphMasterRequests;
    this.tempGraphMasterRequest = this.graphMasterRequest;

    this.presentLoading();
    this.getThreeMonthGraph();
    this.getGraphTat();

    this.dismissLoading();
  }

  getGraphMasterRequest()
  {

    // return;
    const canvas_element_old = document.querySelectorAll('#graph_master_container > div');
    canvas_element_old.forEach((i,v)=>{
      i.remove();
    });

    let count = 0;
    const mainDiv = document.createElement("div");
    mainDiv.style.width = (200 * Number(this.graphMasterRequest.length)) + 'px';

    for (const data of this.graphMasterRequest) {
      const canvasId = 'GRAPH_MASTER_' + count;
      const canvas_element_new = document.createElement("Canvas");
      const ionic_slides = document.createElement("div");
      ionic_slides.style.width = '200px';
      ionic_slides.style.cssFloat = 'left';

      canvas_element_new.setAttribute( "id", canvasId);
      ionic_slides.appendChild(canvas_element_new);
      mainDiv.appendChild(ionic_slides);

      const context = ( <any> canvas_element_new).getContext('2d');
      ++count;

      const graph = new Chart(context, {
        type: 'doughnut',
        data: {
          labels: [
            'Approved: ' + data.APPROVED_TOTAL,
            'Rejected: ' + data.REJECTED_TOTAL,
            'Pending: '  + data.PENDING_TOTAL],
          datasets: [{
            label: data.TICKET_DATE,
            backgroundColor: [
              'green',
              'red',
              'orange',
            ],
            data: [data.APPROVED_TOTAL, data.REJECTED_TOTAL, data.PENDING_TOTAL]
          }]
        },
        options: {
          legend: {
            display: true,
            labels: {
              fontColor: '#3d3d3d'
            },
            position: 'left'
          },
          legendCallback: function(chart) {
            // Return the HTML string here.
          }
        }
      });
    }

    document.querySelector('#graph_master_container').appendChild(mainDiv);
  }

  getThreeMonthGraph() {
    const canvas_element_old = document.querySelectorAll('#graph_three_container > div');
    canvas_element_old.forEach((i,v)=>{
      i.remove();
    });

    let count = 1;
    const mainDiv = document.createElement("div");
    mainDiv.style.width = (200 * Number(this.threeMonthsGraph.length)) + 'px';
    mainDiv.style.overflow = 'hidden';

    for (const data of this.threeMonthsGraph) {
      const canvasId = 'threeMonth_' + count;
      const canvas_element_new = document.createElement("Canvas");
      const ionic_slides = document.createElement("div");

      ionic_slides.style.width = '200px';
      ionic_slides.style.cssFloat = 'left';

      canvas_element_new.setAttribute( "id", canvasId);
      ionic_slides.appendChild(canvas_element_new);
      mainDiv.appendChild(ionic_slides);

      const context = ( <any> canvas_element_new).getContext('2d');
      ++count;

      const graph = new Chart(context, {
        type: 'doughnut',
        data: {
          labels: [
            'Approved: ' + data.APPROVED_TOTAL,
            'Rejected: ' + data.REJECTED_TOTAL,
            'Pending: '  + data.PENDING_TOTAL],
          datasets: [{
            label: data.TICKET_DATE,
            backgroundColor: [
              'green',
              'red',
              'orange',
            ],
            data: [data.APPROVED_TOTAL, data.REJECTED_TOTAL, data.PENDING_TOTAL]
          }]
        },
        options: {
          legend: {
            display: true,
            labels: {
              fontColor: '#3d3d3d'
            },
            position: 'left'
          },
          legendCallback: function(chart) {
            // Return the HTML string here.
          }
        }
      });
    }

    document.querySelector('#graph_three_container').appendChild(mainDiv);
  }

  getGraphTat(){
    // return;
    const canvas_element_old = document.querySelectorAll('#graph_tat_container > div');
    canvas_element_old.forEach((i,v)=>{
      i.remove();
    });

    let count = 1;
    const mainDiv = document.createElement("div");
    mainDiv.style.width = (200 * Number(this.graphTAT.length)) + 'px';
    mainDiv.style.overflow = 'hidden';

    for (const data of this.graphTAT) {
      const canvasId = 'threeMonth_' + count;
      const canvas_element_new = document.createElement("Canvas");
      const ionic_slides = document.createElement("div");
      ionic_slides.style.width = '200px';
      ionic_slides.style.cssFloat = 'left';

      canvas_element_new.setAttribute( "id", canvasId);
      ionic_slides.appendChild(canvas_element_new);
      mainDiv.appendChild(ionic_slides);

      const context = ( <any> canvas_element_new).getContext('2d');
      ++count;

      const graph = new Chart(context, {
        type: 'doughnut',
        data: {
          labels: [
            'Within TAT: ' + data.WHITHIN_TAT,
            'TAT Breached: ' + data.AFTER_TAT
          ],
          datasets: [{
            backgroundColor: [
              'green',
              'red',
            ],
            data: [data.WHITHIN_TAT, data.AFTER_TAT]
          }]
        },
        options: {
          legend: {
            display: true,
            labels: {
              fontColor: '#3d3d3d'
            },
            position: 'left'
          },
          legendCallback: function(chart) {
            // Return the HTML string here.
          }
        }
      });
    }

    document.querySelector('#graph_tat_container').appendChild(mainDiv);
  }

  async showFilterForm(){
    const modal = await this.modalController.create({
      component: ManagerDashboardModalPage,
      componentProps: {},
      cssClass: 'manager_modal_container',
      backdropDismiss: false,
      keyboardClose: false,
      presentingElement: await this.modalController.getTop()
    });
    await modal.present();

    modal.onDidDismiss()
        .then((data) => {
          if (data.data.eventType !== 'save') {
            return;
          }

          this.filterName = data.data.eventName;
          this.presentLoading();
          this.getSubQueryDetailData(data).then((response) => {
            this.graphMasterRequest = response;
            this.dismissLoading();
          });
        });
  }

  async getSubQueryDetailData(data){
    let response = await this.apiService.getSubQueryDetailsAgainstMasterId(this.getSubQueryDetailsParam(data.data.queryId));
    if(response.graphMasterRequests) {
      response = response.graphMasterRequests;
      this.filterDataAvailiable = false;
      return response;
    }
    this.filterDataAvailiable = true;
    return [];
  }

  getEmployeeId(){
    return this.localStorageService.getItem('help_desk_user').employeeId;
  }

  removeFilter(){
    this.presentLoading();
    this.graphMasterRequest = this.tempGraphMasterRequest;
    this.filterName = '';
    this.dismissLoading();
  }

  /* setTimeOutGraphMasterRequest(){
     setTimeout(() => {
       this.getGraphMasterRequest();
     }, 1000);
   }*/

  getSubQueryDetailsParam(id){
    return  {
      requestGraphSubTitle: {
        employeeID: "117480",
        applicationID: "1",
        fromDate: "20200625",
        toDate: "20200703",
        mqID: id,
        dqID:""
      }
    };
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'c_loader',
      message: 'Please wait...',
    });
    await loading.present();
  }

  dismissLoading() {
    setTimeout(() => {
      this.loadingController.dismiss(null, undefined);
    }, 1000);
  }

  isNull(value) {
      return value === null || value === '' || typeof value === 'undefined' || value.toString().length < 1;
  }

}
