import {Component, OnInit} from '@angular/core';
import {LoadingController, MenuController} from '@ionic/angular';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '@app/-common/repositories/user/user.service';
import {AuthenticationService} from '@app/authentication.service';



@Component({
    selector: 'app-portal',
    templateUrl: './portal.page.html',
    styleUrls: ['./portal.page.scss'],
})
export class PortalPage implements OnInit {
    processing = false;
    pageTitle: string;

    constructor(
        private loadingController: LoadingController,
        private menu: MenuController,
        private router: Router,
        private userService: UserService,
        private activatedRoute: ActivatedRoute,
        private authenticationService: AuthenticationService,
    ) {

    }

    ngOnInit() {
      // console.log('portal component:ngOnInit');
    }

    logOut() {
        this.authenticationService.logout().then((res) => {
            this.router.navigate(['./login']);
        });
    }
}