import { Component, OnInit } from '@angular/core';
import {LocalStorageService} from '@app/-common/localStorage.service';
import {DataFlagService} from '@app/data-flag.service';
import {Router, ActivatedRoute} from '@angular/router';
import * as moment from 'moment';
// import {HrListingFilterPipe} from '@app/hr-listing-filter.pipe';

@Component({
  selector: 'app-responder-listing',
  templateUrl: './responder-listing.page.html',
  styleUrls: ['./responder-listing.page.scss'],
})
export class ResponderListingPage implements OnInit {

  responderData: any;
  totalApproved: any;
  totalRejected: any;
  totalPending: any;
  respondData = false;
  filterListingByStatus: any; 
  togglePending: boolean = false;
  toggleApproved: boolean = false;
  toggleRejected: boolean = false;


  constructor(
    private localStorageService: LocalStorageService,
    private dataFlagService: DataFlagService,
    private router: Router,
    ) {
    }
  ionViewDidEnter() {
    this.renderData();
  }
  ngOnInit() {
    this.renderData();
  }

  renderData() {
    this.responderData = this.localStorageService.getItem('hr_responder');

    if (this.responderData.length > 0) {
      this.respondData = true;
      let chartData = this.responderData;
      let date_labels = [];
      let date_approved = [];
      let date_rejected = [];
      let date_pending = [];

      chartData.forEach((i, v) => {
        if (i.TICKET_STATUS.toLowerCase() == 'approved') {
          date_approved.push(1);
        }
        if (i.TICKET_STATUS.toLowerCase() == 'rejected') {
          date_rejected.push(1);
        }
        if (i.TICKET_STATUS.toLowerCase() == 'pending') {
          date_pending.push(1);
        }
      });

      let totalApproved = date_approved.reduce((total, date_approved)=> {
        return total + date_approved;
      }, 0);

      let totalRejected = date_rejected.reduce((total, date_rejected)=> {
        return total + date_rejected;
      }, 0);

      let totalPending = date_pending.reduce((total, date_pending)=> {
        return total + date_pending;
      }, 0);

      this.totalApproved = totalApproved;
      this.totalRejected = totalRejected;
      this.totalPending = totalPending;  
    }
  }

  toggleRequests = (type: string) => {
    if (type.toLowerCase() === 'pending') {
      this.togglePending = !this.togglePending;
      this.toggleRejected = false;
      this.toggleApproved = false;
    }

    if (type.toLowerCase() === 'approved') {
      this.toggleApproved = !this.toggleApproved;
      this.toggleRejected = false;
      this.togglePending = false;
    }
    
    if (type.toLowerCase() === 'rejected') {
      this.toggleRejected = !this.toggleRejected;
      this.toggleApproved = false;
      this.togglePending = false;
    }

    if (this.togglePending || this.toggleApproved || this.toggleRejected) {
      this.filterListingByStatus = type;

    } else {
      this.filterListingByStatus = '';
    }
  }

  showQueryDetail = (id, isResponder) => {
    // Passing Responder check
    this.dataFlagService.changeResponderFlag(isResponder);
    this.router.navigate(['/hr-query-detail', id]);
  }

}
