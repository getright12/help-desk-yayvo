import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ResponderListingPage } from './responder-listing.page';

describe('ResponderListingPage', () => {
  let component: ResponderListingPage;
  let fixture: ComponentFixture<ResponderListingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResponderListingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ResponderListingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
