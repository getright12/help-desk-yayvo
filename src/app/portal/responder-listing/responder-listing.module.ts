import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResponderListingPageRoutingModule } from './responder-listing-routing.module';
// import {HrListingFilterPipe} from '@app/hr-listing-filter.pipe';
import { ResponderListingPage } from './responder-listing.page';
import {SharedModule} from '@app/-common/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    ResponderListingPageRoutingModule
  ],
  // providers: [HrListingFilterPipe],
  declarations: [ResponderListingPage],
  // exports:
})
export class ResponderListingPageModule {}
