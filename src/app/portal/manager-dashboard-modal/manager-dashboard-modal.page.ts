import { Component, OnInit } from '@angular/core';
import { ApiService } from '@app/api.service';
import {ModalController, AlertController} from '@ionic/angular';

@Component({
  selector: 'app-manager-dashboard-modal',
  templateUrl: './manager-dashboard-modal.page.html',
  styleUrls: ['./manager-dashboard-modal.page.scss'],
})
export class ManagerDashboardModalPage implements OnInit {

  queries: any;
  queryTypeId: any;
  queryTypeName: any;

  constructor(private apiService: ApiService, private modalController: ModalController) { }

  ngOnInit() {
    this.getIssueLovData();
  }

  async getIssueLovData()
  {
    this.queries = await this.apiService.getIssuesLov();
    this.queries = this.queries.queryMaster;
  }

  getQueryType(){
    this.dismiss(this.queryTypeId);

  }
  dismiss(id, type = 'save') {
    this.modalController.dismiss({
      queryId: id,
      eventType: type,
      eventName : this.queryTypeName
    });
  }
  cancelModal(){
     this.dismiss(null, 'cancle');
  }
  getQueryEvent(event){
     this.queryTypeId = event.target.value.split(",")[0];
     this.queryTypeName = event.target.value.split(",")[1];
  }
}
