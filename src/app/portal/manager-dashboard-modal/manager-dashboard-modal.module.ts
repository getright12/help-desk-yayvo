import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ManagerDashboardModalPageRoutingModule } from './manager-dashboard-modal-routing.module';

import { ManagerDashboardModalPage } from './manager-dashboard-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ManagerDashboardModalPageRoutingModule
  ],
  declarations: [ManagerDashboardModalPage]
})
export class ManagerDashboardModalPageModule {}
