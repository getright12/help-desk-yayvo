import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManagerDashboardModalPage } from './manager-dashboard-modal.page';

const routes: Routes = [
  {
    path: '',
    component: ManagerDashboardModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManagerDashboardModalPageRoutingModule {}
