import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PortalPageRoutingModule } from './portal-routing.module';

import { PortalPage } from './portal.page';

import {SharedModule} from '../-common/shared.module';
import {ManagerDashboardModalPageModule} from './manager-dashboard-modal/manager-dashboard-modal.module';

@NgModule({
  imports: [
    SharedModule,
    IonicModule,
    PortalPageRoutingModule,
    ManagerDashboardModalPageModule
  ],
  declarations: [PortalPage]
})
export class PortalPageModule {}
