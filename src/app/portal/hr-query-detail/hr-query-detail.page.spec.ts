import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HrQueryDetailPage } from './hr-query-detail.page';

describe('HrQueryDetailPage', () => {
  let component: HrQueryDetailPage;
  let fixture: ComponentFixture<HrQueryDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HrQueryDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HrQueryDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
