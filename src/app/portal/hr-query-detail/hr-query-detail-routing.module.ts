import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HrQueryDetailPage } from './hr-query-detail.page';

const routes: Routes = [
  {
    path: '',
    component: HrQueryDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HrQueryDetailPageRoutingModule {}
