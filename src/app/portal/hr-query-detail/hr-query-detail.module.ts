import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HrQueryDetailPageRoutingModule } from './hr-query-detail-routing.module';

import { HrQueryDetailPage } from './hr-query-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HrQueryDetailPageRoutingModule
  ],
  declarations: [HrQueryDetailPage]
})
export class HrQueryDetailPageModule {}
