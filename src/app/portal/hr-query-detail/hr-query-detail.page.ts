import { LocalStorageService } from './../../-common/localStorage.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {ApiService} from '@app/api.service';
import { promise } from 'protractor';
import {Router, ActivatedRoute } from '@angular/router';
import {DataFlagService} from '@app/data-flag.service';
import {NotificationsService} from '@app/notifications.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-hr-query-detail',
  templateUrl: './hr-query-detail.page.html',
  styleUrls: ['./hr-query-detail.page.scss'],
})
export class HrQueryDetailPage implements OnInit {
  
  ticketID: any;
  hrFlag = false;
  getApiQueryData: any;
  isResponder = false;
  subParams;
  getReponderSub;
  getResponderFlag: any;
  hrRemarks: any;
  getEmployeeID: any;
  remarkList: any;
  previousRoute: any;
  constructor(
    private apiService: ApiService,
    private activatedRoute: ActivatedRoute,
    private dataFlagService: DataFlagService,
    private localStorageService: LocalStorageService,
    private notificationsService: NotificationsService,
    private router: Router,
    public loadingController: LoadingController
    ) {
      // tslint:disable-next-line: max-line-length
      this.previousRoute = !this.localStorageService.getItem('responder_route') ? '/hr-listing' : this.localStorageService.getItem('responder_route');
    }

  ngOnInit() {
    this.subParams = this.activatedRoute.paramMap.subscribe(params => {
      this.ticketID = params.get('id');
    });

   
    this.getEmployeeID = this.localStorageService.getItem('help_desk_user').employeeId;
    // this.getReponderSub = this.dataFlagService.responderFlag.subscribe(getFlag => this.getResponderFlag = getFlag );
    // this.isResponder = this.getResponderFlag === 1 ? true : false;

    // this.hrFlag = this.getResponderFlag === 1 ? true : false;
    this.apiCallgetUserQueryDetails();
  }

  ngOnDestroy() {
    this.subParams.unsubscribe();
    this.getReponderSub.unsubscribe();
  }

  paramObject(ticketID) {
    return {
        getTicketInfoNotify: {
        ticketID: ticketID,
        applicationID: 1
        }
      }
  }

  clickResponderHandler(status) {
    const apiResponse = this.apiCallupdateRequestLog(this.getApiQueryData, status);
    apiResponse.then((res) => {
      if (res.returnStatus.status !== 'SUCCESS') {
        this.notificationsService.toast(res.returnStatus.message);
        return;
      }
      this.updateLocalDataObject(this.getApiQueryData.TICKET_ID, status);
      this.notificationsService.toast(res.returnStatus.message);
      // this.router.navigate(['/hr-listing']);
      this.router.navigateByUrl(this.previousRoute);
    });
  }


  updateLocalDataObject(ticketNo, status) {
    const getResponderList = this.localStorageService.getItem('hr_responder');
    // console.log(getResponderList);
    const updatedData = getResponderList.map(obj  =>
      obj.TICKET_ID === ticketNo ? { ...obj, TICKET_STATUS: status } : obj);
    // update local storage data
    // this.dataFlagService.setResponderData(updatedData);
    this.localStorageService.setItem('hr_responder', updatedData);
  }

  async apiCallupdateRequestLog(data: any, status: string) {
    const params = {
      tcsUpdateLogRequest: {
       ticketID: data.TICKET_ID,
       ticketStatus: status,
       hrRemarks: this.hrRemarks,
       updatedByEmployeeID: this.getEmployeeID
     }
   };
    const updateData = await this.apiService.updateRequestLog(params);
    return updateData;
   }

   async apiCallRemarksList(mqID, dqID) {
    const remarksParams = {remarksListRequest: {mqID, dqID, moduleID: 1}};
    const getRemarksList = await this.apiService.getRemarkList(remarksParams);
    
    if (getRemarksList.returnStatus.status !== 'SUCCESS') {
      this.notificationsService.toast(getRemarksList.returnStatus.message);
      return;
    }
    this.remarkList = !getRemarksList.hrRemarks ? [] : getRemarksList.hrRemarks;
  }

  async apiCallgetUserQueryDetails() {
    this.presentLoading();
    
    const queryData = await this.apiService.getUserQueryDetails(this.paramObject(this.ticketID));
    
    this.getApiQueryData = queryData.returnStatus.status === 'SUCCESS' ? queryData.requestDetails[0] : null;
    
    // Get responder flag
    this.getReponderSub = this.dataFlagService.responderFlag.subscribe(getFlag => this.getResponderFlag = getFlag );
    this.isResponder = this.getResponderFlag === 1 && this.getApiQueryData.TICKET_STATUS === 'Pending' ? true : false;
    this.hrFlag = this.getResponderFlag === 1 && this.getApiQueryData.TICKET_STATUS === 'Pending' ? true : false;
    
    // load hr Remarks on status Pending only
    if (this.getResponderFlag === 1 && this.getApiQueryData.TICKET_STATUS === 'Pending') {
        this.apiCallRemarksList(this.getApiQueryData.MQ_ID, this.getApiQueryData.DQ_ID);
    }

    this.dismissLoading();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'c_loader',
      message: 'Please wait...',
    });
    await loading.present();
  }

  dismissLoading() {
    setTimeout(()=>{
      this.loadingController.dismiss(null, undefined);
    }, 500)
  }
}
