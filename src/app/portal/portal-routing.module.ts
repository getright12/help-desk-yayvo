import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {PortalPage} from './portal.page';

const routes: Routes = [
  {
    path: '',
    component: PortalPage,
    data: {title: 'portal'},
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardPageModule)
      },
      {
        path: 'hr-listing',
        loadChildren: () => import('./hr-listing/hr-listing.module').then( m => m.HrListingPageModule)
      },
      {
        path: 'hr-query-detail/:id',
        loadChildren: () => import('./hr-query-detail/hr-query-detail.module').then( m => m.HrQueryDetailPageModule)
      },
      {
        path: 'new-request',
        loadChildren: () => import('./new-request/new-request.module').then( m => m.NewRequestPageModule)
      },
      {
        path: 'manager-dashboard',
        loadChildren: () => import('./manager-dashboard/manager-dashboard.module').then( m => m.ManagerDashboardPageModule)
      },
      {
        path: 'responder-listing',
        loadChildren: () => import('./responder-listing/responder-listing.module').then( m => m.ResponderListingPageModule)
      },
    ]
  }
  // {
  //   path: 'hr-query-detail',
  //   loadChildren: () => import('./hr-query-detail/hr-query-detail.module').then( m => m.HrQueryDetailPageModule)
  // },

  

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class PortalPageRoutingModule {
}
