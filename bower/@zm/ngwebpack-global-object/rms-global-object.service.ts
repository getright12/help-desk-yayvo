import {Injectable} from '@angular/core';


@Injectable()

export class RMSGlobalObjectService {
    siteObjects = [];

    setSiteObjects(val: any) {
        for (const k in val) {
            if (val.hasOwnProperty(k)) {
                this.siteObjects[k] = val[k];
            }
        }
    }

    getSiteObjects() {
        return this.siteObjects;
    }

    setSiteObject(key: string, val: any) {
        this.siteObjects[key] = val;
    }

    getSiteObject(key: string) {
        return this.siteObjects.hasOwnProperty(key) ? this.siteObjects[key] : '';
    }

    /**
     * getMatch
     * Returns specific key/value data from init/{site}
     *
     * Example:
     * "objStr: course_types, keyStr: id, match: 1" will return 'Double Weekend Seminar' object
     *
     * @param {string} objStr
     * @param {string} keyStr
     * @param {string | number} match
     * @returns {any}
     */
    getMatch(objStr: string, keyStr: string, match: string | number) {
        for (const i of this.siteObjects[objStr]) {
            if (i[keyStr] === match) {
                return i;
            }
        }
    }
}
