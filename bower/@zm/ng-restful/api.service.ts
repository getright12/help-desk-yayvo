import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

import {Observable} from 'rxjs';

import {AppVars} from '@zm/ng-vars/vars.const';


@Injectable()
export class AppApiService {
    readonly appVars = AppVars;

    constructor(
        private http: HttpClient
    ) {
    }

    get(path, args): Observable<any> {
        const paramsData = {
            eAI_MESSAGE: {
                eAI_HEADER: {
                    serviceName: path,
                    client: 'TCS',
                    clientChannel: 'MOB',
                    referenceNum: '',
                    securityInfo: {
                        authentication: {
                            userId: '',
                            password: ''
                        }
                    }
                },
                eAI_BODY: {
                    eAI_REQUEST: {}
                }
            }
        };
        paramsData.eAI_MESSAGE.eAI_HEADER.serviceName = path;
        paramsData.eAI_MESSAGE.eAI_BODY.eAI_REQUEST = args && args.params || null;

        const httpParams = new HttpParams();
        httpParams.append('eAI_MESSAGE', JSON.stringify(paramsData.eAI_MESSAGE));

        return this.http
            .get(this.appVars.apiV1, {
                params: httpParams
            });
    }

    post(path, args): Observable<any> {
        const paramsData = {
            eAI_MESSAGE: {
                eAI_HEADER: {
                    serviceName: path,
                    client: 'TCS',
                    clientChannel: 'MOB',
                    referenceNum: '',
                    securityInfo: {
                        authentication: {
                            userId: '',
                            password: ''
                        }
                    }
                },
                eAI_BODY: {
                    eAI_REQUEST: {}
                }
            }
        };
        paramsData.eAI_MESSAGE.eAI_BODY.eAI_REQUEST = args && args.params || null;

        const httpParams = new HttpParams();
        httpParams.append('eAI_MESSAGE', JSON.stringify(paramsData.eAI_MESSAGE));

        return this.http.post(this.appVars.apiV1, paramsData || null);
    }

    getOther(url): Observable<any>{
        return this.http.get(url, {});
    }

    /* currently not in use */
    patch(path, args?): Observable<any> {
        return this.http.patch(this.appVars.apiV1, args && args.params || null);
    }

    put(path, args?): Observable<any> {
        return this.http.put(this.appVars.apiV1, args && args.params || null);
    }

    delete(path, args?): Observable<any> {
        return this.http.delete(this.appVars.apiV1, args && args.params || null);
    }

    postFormData(url: any, arg: FormData) {
           return this.http.post<any>(url, arg).toPromise();
    }
}
