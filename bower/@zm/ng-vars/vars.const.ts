import {environment} from 'src/environments/environment';

interface ILink {
    generate: (x: string, y: string) => string;
}

interface IJsFileLoad {
    generate: (x: string) => void;
}

export interface IAppVars {
    env: object;
    apiV1: string;
}

export const AppVars: IAppVars = {
    env: environment,
    apiV1: environment.apiUrl
}