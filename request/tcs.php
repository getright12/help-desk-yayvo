<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Accept,Authorization');


$request_body = file_get_contents('php://input');

if ($request_body != null) {

    $curl = curl_init();
    //http://10.252.2.27:7803/tcs/json/gateway/service
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://devapif5.tcscourier.com:7080/tcs/json/gateway/service/",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_SSLCERT  => getcwd() .'/prodcert.pem',
        CURLOPT_SSLCERTPASSWD => 'W3bsph3r3sandbox',
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_POSTFIELDS => $request_body,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    $error_msg = "";
    if (curl_errno($curl)) {
        $error_msg = curl_error($curl);
    }

    curl_close($curl);

    if($response){
        echo $response;
    }else{
        echo json_encode([
            "returnStatus" => [
                "status" => "ERROR",
                "message" => $error_msg
            ]
        ]);
    }
    

}
